#!/usr/bin/python3

import re

def save_results(result):
    """Prints results, saving them to an array so the are only ouput once.

    `result`: `str`
     The grid coordinates to print out.
    """
    if not result in str(results):
        results.append(result)
        print(answer + " " + result)

def check_horizonal(answer, puzzle_line, row, reversed=False):
    """Iterates through the game board horizionally to solve the puzzle.

    `answer`: `str`
     The current word to search for.

    `puzzle_line`: `str`
     The current line of the puzzle to search.

    `row`: `int`
     The current row being searched.

    `reversed`: `bool`
     Is the string reversed? Default: False

    """
    col = puzzle_line.find(answer)
    if not col == -1:
        ## Add the current col to the length of the answer.
        ## Subtract 1 since col starts at 0.
        col_end = str(col + len(answer) - 1)

        ## Was the answer reversed? Change it back before printing.
        if reversed:
            answer = answer[::-1]
            save_results(str(row) + ":" + str(col_end) + " " + str(row) + ":" + str(col))
        else:
            save_results(str(row) + ":" + str(col) + " " + str(row) + ":" + str(col_end))
        
        

def solve_horizonal(answer):
    """Solves the puzzle horizonally, forwards and reverse.
    
    `answer`: `str`
     The current word being searched for.
    """

    row = 0

    for line in puzzle:
        puzzle_line = ""

        ## Create a string from the puzzle line.
        for letter in line:
            puzzle_line += letter
        
        ## Check for the word in the puzzle line.
        check_horizonal(answer, puzzle_line, row)

        ## Check the reverse.
        check_horizonal(answer[::-1], puzzle_line, row, True)
        
        ## Increment row count.
        row += 1

def check_vertical(puzzle_line, row, row_end, row_step):
    """Iterates through the game board vertically to solve the puzzle.

    `puzzle_line`: `str`
     The current line of the puzzle to search.

    `row`: `int`
     The current row being searched.

    `row_end`: `int`
     The end of the puzzle grid.

    `row_step`: `int`:
     The direction to search the grid. l = down; -1 = up.
    """
    ## Store the starting row.
    row_start = row
    
    ## Check for the first character in the puzzle line to
    ## see if we even need to continue searching.
    vertical_answer = answer[0]
    col = puzzle_line.find(vertical_answer)
    
    ## If the character isn't in the puzzle line, exit the function.
    if not col == -1:
        ## Iterate through the rows available.
        for r in range(row + row_step, row_end, row_step):
            
            ## Append the new character to the answer.
            if len(answer) > 1:
                vertical_answer += puzzle[r][col]

            ## If the vertical answer can not be found in the answer, exit the loop.
            if answer.find(vertical_answer) == -1:
                return
            ## Found a match!
            if vertical_answer == answer:
                save_results(str(row_start) + ":" + str(col) + " " + str(r) + ":" + str(col))
                return
            
            

def solve_vertical(answer):
    """Solves the puzzle diagonally, forwards and reverse.
    
    `answer`: `str`
     The current word being searched for.
    """
    row = 0
    
    ## Iterate through the puzzle lines.
    for line in puzzle:
        puzzle_line = ""

        ## Create a string from the puzzle line.
        for letter in line:
            puzzle_line += letter

        ## Check for the string vertically.
        check_vertical(puzzle_line, row, max_row, 1)

        ## Check for the string vertically in reverse.
        check_vertical(puzzle_line, row, -1, -1)

        ## Increment row count.
        row += 1

def check_diagonal(puzzle_line, col_step, row, row_end, row_step):
    """Iterates through the game board diagonally to solve the puzzle.

    `puzzle_line`: `str`
     The current line of the puzzle to search.

    `col_step`: `int`
     The direction to search the grid. l = right; -1 = left.

    `row`: `int`
     The current row being searched.

    `col`: `int`
     The current column being searched.

    `col_step`: `int`:
     The direction to search the grid. l = down; -1 = up.
    """
    ## Get the first char of the answer.
    diagonal_answer = answer[0]

    ## Check the puzzle line to see if the char is in it.
    col = puzzle_line.find(diagonal_answer)

    ## Initalize some variables.
    col_start = 0
    col_end = 0
    
    ## The start row is the previous or next row after the current char,
    ## depending the direction of the search.
    row_start = row + row_step

    ## Are we at the edge of the puzzle yet?
    edge_detection = False
    
    ## If the column step is positive (searching to the right).
    if col_step == 1:
        ## Check if the word is going to go over the end of the puzzle.
        ## No sense in searching if it does.
        edge_detection = len(answer) + col <= max_col

        ## Start the search in the column to the right.
        col_start = col + 1

        ## The ridge edge of the puzzle.
        col_end = max_col

    ## If the column step is negative (searching to the left).
    else:
        ## Check if the word is going to go over the beginning of the puzzle.
        ## No sense in searching if it does.
        ## Check if the diagonal is to the right.
        if col_step == 1:
            edge_detection = len(answer) - col >= -1
        else:
        ## Check if the diagonal is to the left.
            edge_detection = col - len(answer) >= -1

        ## Start the search in the column to the left.
        col_start = col - 1

        ## The left edge of the puzzle.
        col_end = -1

    ## If the character isn't in the puzzle line or the answer is going to be longer than
    ## the puzzle, exit the function.
    if col != -1 and edge_detection:
      
        ## Set the row counter to the row start.
        r = row_start

        ## Iterate through the rows available.
        while True:

            ## Exit the function if the search is off the board.
            if r >= max_row or r <= -1:
                return

            ## Iterate through the columns available.
            for c in range(col_start, col_end, col_step):             

                ## Append the new character to the answer.
                if len(answer) > 1:
                    diagonal_answer += puzzle[r][c]
                
                ## If the diagonal answer can not be found in the answer.
                if answer.find(diagonal_answer) == -1:

                    ## First check to see if the letters exist in the puzzle line string
                    ## after the failure.
                    tmp_puzzle_line = ""

                    ## Create a temp string with the remaining characters.
                    for i in range(col + 1, max_col):
                        tmp_puzzle_line += puzzle[row][i]

                    ## Reset the diagonal answer to the first char of the answer.
                    diagonal_answer = answer[0]

                    ## Check to see if the char is in the string.
                    tmp_col = tmp_puzzle_line.find(diagonal_answer)
                    
                    ## If it doesn't, exit the function.
                    if tmp_col == -1:
                        return
                    ## If it does, reset counters and adjust the column and column start.
                    else:
                        r = row
                        col += 1
                        col_start = col - 1
                        break
                        

                ## Found a match!
                elif diagonal_answer == answer:
                    ## Print the results and exit.
                    save_results(str(row) + ":" + str(col) + " " + str(r) + ":" + str(c))
                    return
                else:
                    ## Increment the row counter.
                    r += row_step

                    ## Exit the function if the search is off the board.
                    if r >= max_row or r <= -1:
                        return
            
            ## Increment the row counter.
            r += row_step

def solve_diagonal(answer):
    """Solves the puzzle diagonally, forwards and reverse.
    
    `answer`: `str`
     The current word being searched for.
    """
    ## Initalize the current row.
    row = 0

    ## Iterate through each line of the puzzle.
    for line in puzzle:
        puzzle_line = ""

        ## Create a string from the puzzle line.
        for letter in line:
            puzzle_line += letter

        ## Check to the right
        check_diagonal(puzzle_line, 1, row, max_row, 1)

        ## Check to the left
        check_diagonal(puzzle_line, -1, row, max_row, 1)

        ## Check to the right
        check_diagonal(puzzle_line, 1, row, -1, -1)

        ## Check to the left
        check_diagonal(puzzle_line, -1, row, -1, -1)
        
        ## Increment the row counter.
        row += 1
        
def read_file(file_name):
    """Reads a alphabet soup input file.

    `file_name`: `str`
     The file to read and parse.
    """
    with open(file_name, "r") as file_input:
        for line in file_input:
            ## Is the line in the format of DIGITxDIGIT?
            if re.search(r"^(\d+)x(\d+)$", line):
                max_row = int(line.split('x')[0])
                max_col = int(line.split('x')[1])
            ## Is the line just a word with a new line?
            elif re.search(r"^(\w)+([^\S \t])$", line):
                answers.append(line.strip())
            ## Is the line a charcter followed by whitespace?
            elif re.search(r"^(\w\s)+$", line):
                puzzle.append(line.split())

    return answers, max_row, max_col, puzzle 

if __name__ == "__main__":
    
    ## The list of answers to search for.
    answers = []

    ## The maximum rows for the grid.
    max_row = 0

    ## The maximum columns for the grid.
    max_col = 0

    ## The puzzle grid.
    puzzle  = []

    ## Saves printed results so they only print once.
    results = []

    ## Parse input.txt.
    answers, max_row, max_col, puzzle = read_file("input.txt")
   
    ## Iterate through the answers.
    for answer in answers:

        ## Try to solve diagonally.
        solve_diagonal(answer)

        ## Try to solve horizonally.
        solve_horizonal(answer)

        ## Try to solve vertically.
        solve_vertical(answer)
